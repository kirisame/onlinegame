﻿//#define _SHADER_TEST

using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using MMD4MecanimProperty = MMD4MecanimImporter.PMX2FBXConfig.MMD4MecanimProperty;

public partial class MMD4MecanimImporter : ScriptableObject
{
	public class MaterialBuilder
	{
		public GameObject		fbxAsset;
		public MMDModel			mmdModel;
		public MMD4MecanimProperty mmd4MecanimProperty;
		
		public string			modelDirectoryName;
		public string			texturesDirectoryName;
		public string			systemDirectoryName;

		public List< Texture >	textureList = new List<Texture>();
		public List< Texture >	systemToonTextureList = new List<Texture>();
		public List< Texture >	systemSA2CTextureList = new List<Texture>();
		public List< Material >	materialList = new List<Material>();
		
		public static readonly string[] textureExtentions = new string[] {
			".bmp", ".png", ".jpg", ".tga"
		};
		
		public static readonly string[] systemToonTextureFileName = new string[] {
			"toon0.bmp",
			"toon01.bmp",
			"toon02.bmp",
			"toon03.bmp",
			"toon04.bmp",
			"toon05.bmp",
			"toon06.bmp",
			"toon07.bmp",
			"toon08.bmp",
			"toon09.bmp",
			"toon10.bmp",
		};

		public enum SA2CTexture
		{
			Pix1,
			Pix2,
			Pix4,
			Pix8,
		}

		public static readonly string[] systemSA2CTextureFileName = new string[] {
			"sa2c1.png",
			"sa2c2.png",
			"sa2c4.png",
			"sa2c8.png",
		};

		//----------------------------------------------------------------------------------------------------------------

		private static bool _HasTextureTransparency( Texture texture )
		{
			if( texture == null ) {
				Debug.LogError("_HasTextureTransparency:Unkonwn Flow.");
				return false;
			}
			
			Texture2D tex2d = texture as Texture2D;
			if( tex2d == null ) {
				Debug.LogError("_HasTextureTransparency:Unkonwn Flow.");
				return false;
			}
			
			Color32[] pixels = tex2d.GetPixels32();
			for( int i = 0; i < pixels.Length; ++i ) {
				if( pixels[i].a != 255 ) {
					return true;
				}
			}
			
			return false;
		}
		
		public static bool HasTextureTransparency( Texture texture )
		{
			if( texture == null ) {
				Debug.LogWarning( "Texture is null." );
				return false;
			}
			
			string textureAssetPath = AssetDatabase.GetAssetPath( texture );
			TextureImporter textureImporter = TextureImporter.GetAtPath( textureAssetPath ) as TextureImporter;
			if( textureImporter == null ) {
				Debug.LogWarning( "Texture is null." );
				return false;
			}

			bool isReadable = textureImporter.isReadable;
			textureImporter.isReadable = true;
			MMD4MecanimEditorCommon.UpdateImportSettings( textureAssetPath );

			bool r = _HasTextureTransparency( texture );
			if( r ) { // Adding alphaIsTransparency
				System.Type t = typeof(TextureImporter);
				var property = t.GetProperty("alphaIsTransparency");
				if( property != null ) { // Unity 4.2.0f4 or Later
					if( (bool)property.GetValue( textureImporter, null ) != true ) {
						property.SetValue( textureImporter, (bool)true, null );
					}
				}
			}

			textureImporter.isReadable = isReadable;
			MMD4MecanimEditorCommon.UpdateImportSettings( textureAssetPath );
			return r;
		}

		private static TextureImporter _GetTextureImporter( Texture texture )
		{
			if( texture == null ) {
				return null;
			}
			
			string textureAssetPath = AssetDatabase.GetAssetPath( texture );
			if( string.IsNullOrEmpty( textureAssetPath ) ) {
				return null;
			}
			
			if( System.IO.Path.GetExtension( textureAssetPath ).ToLower() == ".dds" ) {
				return null;
			}
			
			TextureImporter textureImporter = TextureImporter.GetAtPath( textureAssetPath ) as TextureImporter;
			if( textureImporter == null ) {
				Debug.LogWarning( "Texture is null." );
				return null;
			}

			return textureImporter;
		}

		public static void SetTextureAdvanced( Texture texture, bool isAdvanced )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}
		}

		public static void SetTextureWrapMode( Texture texture, TextureWrapMode textureWrapMode )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}

			if( textureImporter.wrapMode != textureWrapMode ) {
				textureImporter.wrapMode = textureWrapMode;
				MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
			}
		}

		public static void SetTextureAlphaIsTransparency( Texture texture, bool alphaIsTransparency )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}

			{
				System.Type t = typeof(TextureImporter);
				var property = t.GetProperty("alphaIsTransparency");
				if( property != null ) { // Unity 4.2.0f4 or Later
					if( (bool)property.GetValue( textureImporter, null ) != alphaIsTransparency ) {
						property.SetValue( textureImporter, alphaIsTransparency, null );
						MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
					}
				}
			}
		}

		public static void SetTextureMipmapEnabled( Texture texture, bool mipmapEnabled )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}

			if( textureImporter.mipmapEnabled != mipmapEnabled ) {
				textureImporter.mipmapEnabled = mipmapEnabled;
				MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
			}
		}

		public static void SetTextureFilterMode( Texture texture, FilterMode filterMode )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}

			if( textureImporter.filterMode != filterMode ) {
				textureImporter.filterMode = filterMode;
				MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
			}
		}

		public static void SetTextureType( Texture texture, TextureImporterType textureType )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}
			
			if( textureImporter.textureType != textureType ) {
				textureImporter.textureType = textureType;
				MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
			}
		}

		public static void SetTextureFormat( Texture texture, TextureImporterFormat textureFormat )
		{
			TextureImporter textureImporter = _GetTextureImporter( texture );
			if( textureImporter == null ) {
				return;
			}
			
			if( textureImporter.textureFormat != textureFormat ) {
				textureImporter.textureFormat = textureFormat;
				MMD4MecanimEditorCommon.UpdateImportSettings( AssetDatabase.GetAssetPath( texture ) );
			}
		}

		//----------------------------------------------------------------------------------------------------------------
		
		private MMD4MecanimEditorCommon.TextureAccessor _GetTextureAccessor( int textureID, MMD4MecanimProperty mmd4MecanimProperty )
		{
			if( mmd4MecanimProperty.transparency == PMX2FBXConfig.Transparency.Disable ) {
				return null;
			}

			if( (uint)textureID < (uint)this.textureList.Count && this.textureList[textureID ] != null ) {
				MMD4MecanimCommon.TextureFileSign textureFileSign = MMD4MecanimCommon.GetTextureFileSign( AssetDatabase.GetAssetPath( this.textureList[textureID ] ) );
				if( textureFileSign == MMD4MecanimCommon.TextureFileSign.None || /* for File not foud / File can't open */
					textureFileSign == MMD4MecanimCommon.TextureFileSign.BmpWithAlpha ||
					textureFileSign == MMD4MecanimCommon.TextureFileSign.TargaWithAlpha ||
					textureFileSign == MMD4MecanimCommon.TextureFileSign.PngWithAlpha ) {
					return new MMD4MecanimEditorCommon.TextureAccessor( this.textureList[textureID ] ); /* Check Alpha and set alphaIsTransparency */
				}
			}
			
			return null;
		}
		
		private bool _IsMaterialTransparency( MMDModel.Material xmlMaterial, MMD4MecanimProperty mmd4MecanimProperty )
		{
			if( mmd4MecanimProperty.transparency == PMX2FBXConfig.Transparency.Disable ) {
				return false;
			}
			if( xmlMaterial.diffuse.a < 1.0f - Mathf.Epsilon ) {
				return true;
			}
			
			return false;
		}
		
		private Shader _GetShader( MMDModel.Material xmlMaterial, MMD4MecanimProperty mmd4MecanimProperty, bool isTransparency )
		{
			System.Text.StringBuilder shaderNameBuilder = new System.Text.StringBuilder();
#if MMD4MECANIM_DEBUG
			if( mmd4MecanimProperty.isDebugShader ) {
				shaderNameBuilder.Append( "MMD4Mecanim/MMDLit-Test" );
				shaderNameBuilder.Append( "-Deferred" );
				shaderNameBuilder.Append( "-Transparent" );
				if( mmd4MecanimProperty.isDrawEdge ) {
					if( xmlMaterial.isDrawEdge ) {
						shaderNameBuilder.Append( "-Edge" );
					}
				}

				return Shader.Find( shaderNameBuilder.ToString() );
			}
#endif
			if( mmd4MecanimProperty.isDeferred ) {
				shaderNameBuilder.Append( "MMD4Mecanim/Deferred/MMDLit" );
			} else {
				shaderNameBuilder.Append( "MMD4Mecanim/MMDLit" );
			}

			if( !xmlMaterial.isDrawSelfShadowMap ) {
				shaderNameBuilder.Append( "-NoShadowCasting" );
			}
			if( xmlMaterial.isDrawBothFaces ) {
				shaderNameBuilder.Append( "-BothFaces" );
			}
			if( mmd4MecanimProperty.transparency != PMX2FBXConfig.Transparency.Disable ) {
				if( isTransparency ) {
					shaderNameBuilder.Append( "-Transparent" );
				}
			}
			if( mmd4MecanimProperty.isDrawEdge ) {
				if( xmlMaterial.isDrawEdge ) {
					shaderNameBuilder.Append( "-Edge" );
				}
			}
			return Shader.Find( shaderNameBuilder.ToString() );
		}
		
		private static string _PathCombine( string pathA, string pathB )
		{
			//return Path.Combine( pathA, pathB );
			if( string.IsNullOrEmpty( pathA ) ) {
				return pathB;
			}
			if( string.IsNullOrEmpty( pathB ) ) {
				return pathA;
			}
			
			System.Text.StringBuilder str = new System.Text.StringBuilder();
			for( int i = 0; i < pathA.Length; ++i ) {
				if( pathA[i] == '\\' ) {
					str.Append( '/' );
				} else if( pathA[i] == '.' ) {
					if( i + 1 < pathA.Length && ( pathA[i + 1] == '/' || pathA[i + 1] == '\\' ) ) {
						++i;
					} else {
						str.Append( pathA[i] );
					}
				} else {
					str.Append( pathA[i] );
				}
			}
			
			if( pathA[pathA.Length - 1] != '/' && pathA[pathA.Length - 1] != '\\' ) {
				str.Append( '/' );
			}

			for( int i = 0; i < pathB.Length; ++i ) {
				if( pathB[i] == '\\' ) {
					str.Append( '/' );
				} else if( pathB[i] == '.' ) {
					if( i + 1 < pathB.Length && ( pathB[i + 1] == '/' || pathB[i + 1] == '\\' ) ) {
						++i;
					} else {
						str.Append( pathB[i] );
					}
				} else {
					str.Append( pathB[i] );
				}
			}

			return str.ToString();
		}

		private Texture _LoadTexture( string directoryName, string fileName )
		{
			if( string.IsNullOrEmpty( fileName ) ) {
				return null;
			}
			
			string textureAssetPath = _PathCombine( directoryName, fileName );
			//Debug.Log( "Try to open:" + textureAssetPath );
			Texture texture = AssetDatabase.LoadAssetAtPath( textureAssetPath, typeof(Texture) ) as Texture;
			if( texture != null ) {
				return texture;
			}
			
			string extension = System.IO.Path.GetExtension( fileName ).ToLower();
			if( extension == ".spa" || extension == ".sph" ) {
				for( int i = 0; i < textureExtentions.Length; ++i ) {
					string newTextureAssetPath = textureAssetPath + textureExtentions[i];
					texture = AssetDatabase.LoadAssetAtPath( newTextureAssetPath, typeof(Texture) ) as Texture;
					if( texture != null ) {
						return texture;
					}
				}

				MMD4MecanimCommon.TextureFileSign textureFileSign = MMD4MecanimCommon.GetTextureFileSign( textureAssetPath );
				if( textureFileSign != MMD4MecanimCommon.TextureFileSign.None ) {
					string newTextureAssetPath = textureAssetPath;
					switch( textureFileSign ) {
					case MMD4MecanimCommon.TextureFileSign.Bmp:
					case MMD4MecanimCommon.TextureFileSign.BmpWithAlpha:
						newTextureAssetPath += ".bmp";
						break;
					case MMD4MecanimCommon.TextureFileSign.Png:
					case MMD4MecanimCommon.TextureFileSign.PngWithAlpha:
						newTextureAssetPath += ".png";
						break;
					case MMD4MecanimCommon.TextureFileSign.Jpeg:
						newTextureAssetPath += ".jpg";
						break;
					case MMD4MecanimCommon.TextureFileSign.Targa:
					case MMD4MecanimCommon.TextureFileSign.TargaWithAlpha:
						newTextureAssetPath += ".tga";
						break;
					default:
						Debug.LogWarning( "_LoadTexture: Unknown .sph/.spa file format. " + textureAssetPath );
						return null;
					}
					
					if( !System.IO.File.Exists( newTextureAssetPath ) ) {
						if( directoryName == this.texturesDirectoryName ) {
							System.IO.File.Move( textureAssetPath, newTextureAssetPath );
						} else {
							System.IO.File.Copy( textureAssetPath, newTextureAssetPath );
						}
						
						AssetDatabase.ImportAsset( newTextureAssetPath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport );
					} else {
						Debug.LogWarning( "_LoadTexture: Unknown flow." );
					}
					
					texture = AssetDatabase.LoadAssetAtPath( newTextureAssetPath, typeof(Texture) ) as Texture;
					if( texture != null ) {
						return texture;
					}
				}
			}

			return null;
		}

		private static string _PrefixFileName( string fileName )
		{
			if( fileName != null ) {
				int endPos = fileName.Length - 1;
				for( ; endPos >= 0; --endPos ) {
					char c = fileName[endPos];
					if( c != ' ' && c != '\r' && c != '\n' && c != '\t' ) {
						break;
					}
				}
				
				return fileName.Substring( 0, endPos + 1 );
			}
			
			return fileName;
		}

		private Texture _LoadTexture( string fileName )
		{
			fileName = _PrefixFileName( fileName );

			Texture texture = _LoadTexture( this.texturesDirectoryName, fileName );
			if( texture != null ) {
				return texture;
			}
			texture = _LoadTexture( this.modelDirectoryName, fileName );
			if( texture != null ) {
				return texture;
			}
			texture = _LoadTexture( this.systemDirectoryName, fileName );
			if( texture != null ) {
				return texture;
			}

			Debug.Log( "_LoadTexture: File not found. " + fileName );
			return null;
		}
		
		public void Build()
		{
			if( this.fbxAsset == null ) {
				Debug.LogError( "Not found FBX." );
				return;
			}
			
			if( this.mmdModel == null ) {
				Debug.LogError( "Not found model.xml" );
				return;
			}
			
			this.modelDirectoryName = System.IO.Path.GetDirectoryName( AssetDatabase.GetAssetPath( this.fbxAsset ) ) + "/";
			this.texturesDirectoryName = this.modelDirectoryName + "Textures/";
			string shaderDirectoryName = System.IO.Path.GetDirectoryName( AssetDatabase.GetAssetPath( Shader.Find( "MMD4Mecanim/MMDLit" ) ) );
			this.systemDirectoryName = System.IO.Path.Combine( System.IO.Path.GetDirectoryName( shaderDirectoryName ), "Textures" ) + "/";
			
			if( this.mmdModel.textureList != null ) {
				int textureListLength = this.mmdModel.textureList.Length;
				bool[] usedTextureIDList = new bool[textureListLength];
				if( this.mmdModel.materialList != null ) {
					for( int i = 0; i < this.mmdModel.materialList.Length; ++i ) {
						int textureID = this.mmdModel.materialList[i].textureID;
						int toonTextureID = this.mmdModel.materialList[i].toonTextureID;
						int additionalTextureID = this.mmdModel.materialList[i].additionalTextureID;
						if( (uint)textureID < (uint)textureListLength ) {
							usedTextureIDList[textureID] = true;
						}
						if( (uint)toonTextureID < (uint)textureListLength ) {
							usedTextureIDList[toonTextureID] = true;
						}
						if( (uint)additionalTextureID < (uint)textureListLength ) {
							usedTextureIDList[additionalTextureID] = true;
						}
					}
				}
				
				for( int i = 0; i < this.mmdModel.textureList.Length; ++i ) {
					if( usedTextureIDList[i] ) {
						string textureFileName = this.mmdModel.textureList[i].fileName;
						Texture texture = _LoadTexture( textureFileName );
						if( texture != null ) {
							this.textureList.Add( texture );
						} else {
							Debug.LogWarning( "Not found texture. " + i + ":" + textureFileName );
							this.textureList.Add( null );
						}
					} else {
						this.textureList.Add( null );
					}
				}
			}
			
			for( int i = 0; i < systemToonTextureFileName.Length; ++i ) {
				Texture texture = _LoadTexture( this.systemDirectoryName, systemToonTextureFileName[i] );
				if( texture != null ) {
					this.systemToonTextureList.Add( texture );
				} else {
					Debug.LogWarning( "Not found system toon texture. " + i + ":" + systemToonTextureFileName[i] );
					this.systemToonTextureList.Add( null );
				}
			}
			
			for( int i = 0; i < systemSA2CTextureFileName.Length; ++i ) {
				Texture texture = _LoadTexture( this.systemDirectoryName, systemSA2CTextureFileName[i] );
				if( texture != null ) {
					this.systemSA2CTextureList.Add( texture );
				} else {
					Debug.LogWarning( "Not found system toon texture. " + i + ":" + systemSA2CTextureFileName[i] );
					this.systemSA2CTextureList.Add( null );
				}
			}

			float edgeScale = 1.0f;
			float shadowLum = 1.25f;
			float selfShadowStr = 0.0f;
			float lambertStr = 0.0f;
			float addLambertStr = 0.0f;
			if( this.mmd4MecanimProperty != null ) {
				edgeScale = this.mmd4MecanimProperty.edgeScale;
				shadowLum = this.mmd4MecanimProperty.shadowLum;
				selfShadowStr = this.mmd4MecanimProperty.isSelfShadow ? 1.0f : 0.0f;
				lambertStr = this.mmd4MecanimProperty.lambertStr;
				addLambertStr = this.mmd4MecanimProperty.addLambertStr;
			}
			
			List<MMD4MecanimEditorCommon.TextureAccessor> textureAccessorList = new List<MMD4MecanimEditorCommon.TextureAccessor>();
			for( int i = 0; i < this.textureList.Count; ++i ) {
				textureAccessorList.Add( _GetTextureAccessor( i, this.mmd4MecanimProperty ) );
			}
			
			this.materialList = MMD4MecanimImporter._GetFBXMaterialList( this.fbxAsset );
			MMD4MecanimEditorCommon.MMDMesh mmdMesh = new MMD4MecanimEditorCommon.MMDMesh( this.fbxAsset );
			if( this.materialList != null ) {
				bool[] materialIsTransparency = new bool[this.materialList.Count];
				for( int i = 0; i < this.materialList.Count; ++i ) {
					if( this.mmdModel.materialList == null || i >= this.mmdModel.materialList.Length ) {
						Debug.LogError( "Out of range." + i + "/" + this.mmdModel.materialList.Length );
						continue;
					}
					
					materialIsTransparency[i] = _IsMaterialTransparency( this.mmdModel.materialList[i], this.mmd4MecanimProperty );
					if( !materialIsTransparency[i] ) {
						int textureID = this.mmdModel.materialList[i].textureID;
						if( (uint)textureID < (uint)this.textureList.Count && textureAccessorList[textureID] != null ) {
							if( textureAccessorList[textureID].isTransparency ) {
								if( mmdMesh.CheckTransparency( i, textureAccessorList[textureID] ) ) {
									materialIsTransparency[i] = true;
								}
							}
						}
					}
				}

				for( int i = 0; i < this.materialList.Count; ++i ) {
					if( this.materialList[i] == null ) {
						Debug.LogWarning( "Not found material." + i );
						continue;
					}
					if( this.mmdModel.materialList == null || i >= this.mmdModel.materialList.Length ) {
						Debug.LogError( "Out of range." + i + "/" + this.mmdModel.materialList.Length );
						continue;
					}
					
					Material material = this.materialList[i];
					MMDModel.Material xmlMaterial = this.mmdModel.materialList[i];
					
					material.shader = _GetShader( xmlMaterial, this.mmd4MecanimProperty, materialIsTransparency[i] );
					
					material.SetColor("_Color", xmlMaterial.diffuse);
					material.SetColor("_Specular", xmlMaterial.specular);
					material.SetFloat("_Shininess", xmlMaterial.shiness);
					material.SetColor("_Ambient", xmlMaterial.ambient);
					material.SetFloat("_ShadowLum", shadowLum);
					if( xmlMaterial.isDrawSelfShadow ) {
						material.SetFloat("_SelfShadowStr", selfShadowStr);
					} else {
						material.SetFloat("_SelfShadowStr", 0.0f);
					}
					material.SetFloat("_LambertStr", lambertStr);
					material.SetFloat("_AddLambertStr", addLambertStr);
					material.SetColor("_EdgeColor", xmlMaterial.edgeColor);
					material.SetFloat("_EdgeSize", xmlMaterial.edgeSize * edgeScale);

					if( this.mmd4MecanimProperty != null && this.mmd4MecanimProperty.isDeferred ) {
						Texture sa2cTexture = this.systemSA2CTextureList[(int)SA2CTexture.Pix4];
						SetTextureWrapMode( sa2cTexture, TextureWrapMode.Repeat );
						SetTextureAlphaIsTransparency( sa2cTexture, true );
						SetTextureMipmapEnabled( sa2cTexture, false );
						SetTextureFilterMode( sa2cTexture, FilterMode.Point );
						SetTextureType( sa2cTexture,TextureImporterType.Advanced );
						SetTextureFormat( sa2cTexture, TextureImporterFormat.Alpha8 );
						//material.SetTexture("_DefSA2CTex", sa2cTexture);
						//material.SetFloat("_DefSA2CSize", 8.0f);
						material.SetTexture("_DefSA2CTex", sa2cTexture);
						material.SetFloat("_DefSA2CSize", 4.0f);
						//material.SetTexture("_DefSA2CTex", sa2cTexture);
						//material.SetFloat("_DefSA2CSize", 2.0f);
					}

					Texture texture = (xmlMaterial.textureID >= 0) ? this.textureList[xmlMaterial.textureID] : null;
					SetTextureWrapMode( texture, TextureWrapMode.Repeat );
					material.mainTexture = texture;
					material.mainTextureScale = new Vector2(1, 1);
					material.mainTextureOffset = new Vector2(0, 0);

					Texture additionalTexture = (xmlMaterial.additionalTextureID >= 0) ? this.textureList[xmlMaterial.additionalTextureID] : null;
					if( xmlMaterial.sphereMode == MMDModel.SphereMode.Multiply ) {
						SetTextureWrapMode( additionalTexture, TextureWrapMode.Clamp );
						material.SetTexture("_SphereMulTex", additionalTexture);
						material.SetTextureScale("_SphereMulTex", new Vector2(1, 1));
						material.SetTextureOffset("_SphereMulTex", new Vector2(0, 0));
					} else if( xmlMaterial.sphereMode == MMDModel.SphereMode.Adding ) {
						SetTextureWrapMode( additionalTexture, TextureWrapMode.Clamp );
						material.SetTexture("_SphereAddTex", additionalTexture);
						material.SetTextureScale("_SphereAddTex", new Vector2(1, 1));
						material.SetTextureOffset("_SphereAddTex", new Vector2(0, 0));
					} else if( xmlMaterial.sphereMode == MMDModel.SphereMode.SubTexture ) {
						// Not supported.
						Debug.LogWarning( "Material [" + i + ":" + xmlMaterial.nameJp + "] Not supported SubTexture." );
					}
	
					Texture toonTexture = (xmlMaterial.toonTextureID >= 0) ? this.textureList[xmlMaterial.toonTextureID] : null;
					if( toonTexture == null && xmlMaterial.toonID > 0 ) {
						//Debug.Log( xmlMaterial.toonID );
						toonTexture = this.systemToonTextureList[xmlMaterial.toonID];
					}
					if( toonTexture != null ) {
						SetTextureWrapMode( toonTexture, TextureWrapMode.Clamp );
					}
					material.SetTexture("_ToonTex", toonTexture);
					material.SetTextureScale("_ToonTex", new Vector2(1, 1));
					material.SetTextureOffset("_ToonTex", new Vector2(0, 0));
					
					material.renderQueue = 2001 + i; // memo: No Effects.
				}
			}
		}
	}
	
	public class IndexBuilder
	{
		public GameObject	fbxAsset;
		public MMDModel		mmdModel;

		public void _Write( System.IO.MemoryStream memoryStream, uint value )
		{
			byte[] bytes = new byte[4] {
				(byte)((value) & 0xff),
				(byte)((value >> 8) & 0xff),
				(byte)((value >> 16) & 0xff),
				(byte)((value >> 24) & 0xff),
			};
			
			memoryStream.Write( bytes, 0, 4 );
		}
		
		private SkinnedMeshRenderer[] _cache_skinnedMeshRenderers = null;

		public void Build()
		{
			if( this.fbxAsset == null ) {
				Debug.LogError( "Not found FBX." );
				return;
			}
			if( this.mmdModel == null ) {
				Debug.LogError( "Not found model.xml" );
				return;
			}
			
			string indexAssetPath = MMD4MecanimImporter.GetIndexDataPath( AssetDatabase.GetAssetPath( this.fbxAsset ) );
			
			uint numVertex = this.mmdModel.globalSettings.numVertex;
			List<uint>[] indexTable = new List<uint>[numVertex];
			for( uint i = 0; i < numVertex; ++i ) {
				indexTable[i] = new List<uint>();
			}
			
			/*
				meshIndex = (uint)indexTable[morphVertexIndex] >> 24;
				vertexIndex = (uint)indexTable[morphVertexIndex] & 0x00ffffff;
			*/
			
			uint meshLength = 0;
			uint colorLength = 0;

			_cache_skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers( this.fbxAsset );
			if( _cache_skinnedMeshRenderers != null ) {
				meshLength = (uint)_cache_skinnedMeshRenderers.Length;
				for( int meshIndex = 0; meshIndex < meshLength; ++meshIndex ) {
					Color32[] colors = _cache_skinnedMeshRenderers[meshIndex].sharedMesh.colors32;
					if( colors != null ) {
						colorLength += (uint)colors.Length;
						for( uint i = 0; i < (uint)colors.Length; ++i ) {
							uint index = (uint)colors[i].r | ((uint)colors[i].g << 8) | ((uint)colors[i].b << 16) | ((uint)colors[i].a << 24);
							if( index < numVertex ) {
								indexTable[index].Add( ((uint)meshIndex << 24) | i );
							}
						}
					}
				}
			}

			System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
			_Write( memoryStream, numVertex );
			_Write( memoryStream, (meshLength << 24) | colorLength );
			uint offset = 2 + numVertex + 1;
			for( uint i = 0; i < numVertex; ++i ) {
				_Write( memoryStream, offset );
				offset += (uint)indexTable[i].Count;
			}
			_Write( memoryStream, offset );
			for( uint i = 0; i < numVertex; ++i ) {
				for( int j = 0; j < indexTable[i].Count; ++j ) {
					_Write( memoryStream, indexTable[i][j] );
				}
			}
			memoryStream.Flush();
			
			FileStream indexStream = File.Open( indexAssetPath, FileMode.Create, FileAccess.Write, FileShare.None );
			byte[] memoryStreamArray = memoryStream.ToArray();
			indexStream.Write( memoryStreamArray, 0, (int)memoryStream.Length );
			indexStream.Close();

			AssetDatabase.ImportAsset( indexAssetPath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport );
		}
	}
	
	private void _OnInspectorGUI_Material( Material material )
	{
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.TextField("Material");
		EditorGUILayout.EndHorizontal();
	}

	private static int _ComputeFBXMaterialList(
		Material[] materials,
		string fbxAssetDirectoryName,
		Dictionary<int, Material> materialDict )
	{
		int materialCount = 0;
		if( materials != null ) {
			foreach( Material material in materials ) {
				if( material == null ) {
					continue;
				}
				string materialPath = AssetDatabase.GetAssetPath( material );
				if( fbxAssetDirectoryName == Path.GetDirectoryName( Path.GetDirectoryName( materialPath ) ) ) {
					string materialName = Path.GetFileNameWithoutExtension( materialPath );
					int materialValue = MMD4MecanimCommon.ToInt( materialName );
					materialDict[materialValue] = material;
					materialCount = ((materialValue + 1) > materialCount) ? (materialValue + 1) : materialCount;
				}
			}
		}

		return materialCount;
	}

	public static List<Material> _GetFBXMaterialList( GameObject fbxAsset )
	{
		if( fbxAsset == null ) {
			return null;
		}

		string fbxAssetPath = AssetDatabase.GetAssetPath( fbxAsset );
		string fbxAssetDirectoryName = Path.GetDirectoryName( fbxAssetPath );

		Dictionary<int, Material> materialDict = new Dictionary<int, Material>();
		
		int materialCount = 0;
		MeshRenderer[] meshRenderers = MMD4MecanimCommon.GetMeshRenderers( fbxAsset );
		if( meshRenderers != null ) {
			foreach( MeshRenderer meshRenderer in meshRenderers ) {
				int tempMaterialCount = _ComputeFBXMaterialList( meshRenderer.sharedMaterials, fbxAssetDirectoryName, materialDict );
				if( materialCount < tempMaterialCount ) {
					materialCount = tempMaterialCount;
				}
			}
		}

		SkinnedMeshRenderer[] skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers( fbxAsset );
		if( skinnedMeshRenderers != null ) {
			foreach( SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers ) {
				int tempMaterialCount = _ComputeFBXMaterialList( skinnedMeshRenderer.sharedMaterials, fbxAssetDirectoryName, materialDict );
				if( materialCount < tempMaterialCount ) {
					materialCount = tempMaterialCount;
				}
			}
		}

		List<Material> materialList = new List<Material>();
		for( int i = 0; i < materialCount; ++i ) {
			Material material = null;
			if( materialDict.TryGetValue( i, out material ) ) {
				materialList.Add( material );
			} else {
				materialList.Add( null );
			}
		}
		
		return materialList;
	}

	private void _OnInspectorGUI_Material()
	{
		if( this.pmx2fbxConfig == null || this.pmx2fbxConfig.mmd4MecanimProperty == null ) {
			return;
		}

		GUILayout.Label( "FBX", EditorStyles.boldLabel );
		GUILayout.BeginHorizontal();
		GUILayout.Space( 26.0f );
		_OnInspectorGUI_ShowFBXField();
		GUILayout.EndHorizontal();
		
		EditorGUILayout.Separator();

		var mmd4MecanimProperty = this.pmx2fbxConfig.mmd4MecanimProperty;

#if MMD4MECANIM_DEBUG
		mmd4MecanimProperty.isDebugShader = EditorGUILayout.Toggle( "Debug", mmd4MecanimProperty.isDebugShader );
#endif
		mmd4MecanimProperty.isDeferred = EditorGUILayout.Toggle( "Deferred", mmd4MecanimProperty.isDeferred );
		mmd4MecanimProperty.transparency = (PMX2FBXConfig.Transparency)EditorGUILayout.EnumPopup( "Transparency", (System.Enum)mmd4MecanimProperty.transparency );
		mmd4MecanimProperty.shadowLum = EditorGUILayout.FloatField( "ShadowLum", mmd4MecanimProperty.shadowLum );
		mmd4MecanimProperty.isSelfShadow = EditorGUILayout.Toggle( "SelfShadow", mmd4MecanimProperty.isSelfShadow );
		mmd4MecanimProperty.lambertStr = EditorGUILayout.FloatField( "LambertStr", mmd4MecanimProperty.lambertStr );
		mmd4MecanimProperty.addLambertStr = EditorGUILayout.FloatField( "AddLambertStr", mmd4MecanimProperty.addLambertStr );
		mmd4MecanimProperty.isDrawEdge = EditorGUILayout.Toggle( "DrawEdge", mmd4MecanimProperty.isDrawEdge );
		mmd4MecanimProperty.edgeScale = EditorGUILayout.FloatField( "EdgeScale", mmd4MecanimProperty.edgeScale );

		EditorGUILayout.Separator();
		
		// Search Materials
		
		EditorGUILayout.Separator();

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if( GUILayout.Button("Process") ) {
			SavePMX2FBXConfig();
			Process();
		}
		GUILayout.EndHorizontal();
	}
	
	public void Process()
	{
		_ProcessMaterial( this.fbxAsset, this.mmdModel, this.pmx2fbxConfig );
		Debug.Log( "Processed." );
	}
	
	private bool _initializeMaterialAtLeastOnce;
	private bool _initializeMaterialAfterPMX2FBX;

	static bool _CheckFBXMaterial( Material[] materials )
	{
		if( materials != null ) {
			foreach( Material material in materials ) {
				if( material != null ) {
					if( material.shader != null && material.shader.name.StartsWith("MMD4Mecanim") ) {
						return true;
					} else if( material.mainTexture != null ) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public void _CheckFBXMaterial()
	{
		if( this.fbxAsset == null || this.mmdModel == null || this.pmx2fbxConfig == null ) {
			return;
		}
		
		bool initializeMaterial = false;
		bool initializeIndex = false;

		MeshRenderer[] meshRenderers = MMD4MecanimCommon.GetMeshRenderers( fbxAsset.gameObject );
		SkinnedMeshRenderer[] skinnedMeshRenderers = MMD4MecanimCommon.GetSkinnedMeshRenderers( fbxAsset.gameObject );

		if( !_initializeMaterialAtLeastOnce ) {
			if( _initializeMaterialAfterPMX2FBX ) {
				initializeMaterial = true;
			} else {
				bool initializedAnything = false;

				if( meshRenderers != null ) {
					foreach( MeshRenderer meshRenderer in meshRenderers ) {
						initializedAnything |= _CheckFBXMaterial( meshRenderer.sharedMaterials );
						if( initializedAnything ) {
							break;
						}
					}
				}
				if( initializedAnything == false ) {
					if( skinnedMeshRenderers != null ) {
						foreach( SkinnedMeshRenderer skinnedMeshRenderer in skinnedMeshRenderers ) {
							initializedAnything |= _CheckFBXMaterial( skinnedMeshRenderer.sharedMaterials );
							if( initializedAnything ) {
								break;
							}
						}
					}
				}

				initializeMaterial = !initializedAnything;
			}
		}
		
		if( skinnedMeshRenderers != null && skinnedMeshRenderers.Length > 0 ) {
			if( this.indexData != null ) {
				if( !MMD4MecanimData.ValidateIndexData( this.indexData, skinnedMeshRenderers ) ) {
					this.indexData = null;
					initializeIndex = true;
				}
			} else {
				initializeIndex = true;
			}
		}
		
		if( initializeMaterial || initializeIndex ) {
			if( initializeMaterial ) {
				Debug.Log( "MMD4Mecanim:Initialize FBX Material:" + this.fbxAsset.name );
				_initializeMaterialAtLeastOnce = true;
				_ProcessMaterial( fbxAsset, mmdModel, pmx2fbxConfig );
			}
			if( initializeIndex ) {
				Debug.Log( "MMD4Mecanim:Initialize FBX Index:" + this.fbxAsset.name );
				_ProcessIndex( fbxAsset, mmdModel );
				
				this.indexAsset = AssetDatabase.LoadAssetAtPath(
					GetIndexDataPath( this.fbxAssetPath ), typeof(TextAsset) ) as TextAsset;
				if( this.indexAsset != null ) {
					this.indexData = MMD4MecanimData.BuildIndexData( this.indexAsset );
				}
			}
		}
	}
	
	private static void _ProcessMaterial( GameObject fbxAsset, MMDModel mmdModel, PMX2FBXConfig pmx2fbxConfig )
	{
		if( fbxAsset == null ) {
			Debug.LogError( "FBXAsset is null." );
			return;
		}
		if( mmdModel == null ) {
			Debug.LogError( "MMDModel is null." );
			return;
		}
		if( pmx2fbxConfig == null ) {
			Debug.LogError( "pmx2fbxConfig is null." );
			return;
		}
		
		MaterialBuilder materialBuilder = new MaterialBuilder();
		materialBuilder.fbxAsset = fbxAsset;
		materialBuilder.mmdModel = mmdModel;
		materialBuilder.mmd4MecanimProperty = pmx2fbxConfig.mmd4MecanimProperty;
		materialBuilder.Build();
	}

	private static void _ProcessIndex( GameObject fbxAsset, MMDModel mmdModel )
	{
		if( fbxAsset == null ) {
			Debug.LogError( "FBXAsset is null." );
			return;
		}
		if( mmdModel == null ) {
			Debug.LogError( "MMDModel is null." );
			return;
		}

		IndexBuilder indexBuilder = new IndexBuilder();
		indexBuilder.fbxAsset = fbxAsset;
		indexBuilder.mmdModel = mmdModel;
		indexBuilder.Build();
	}
}

